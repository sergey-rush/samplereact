﻿using Microsoft.EntityFrameworkCore;

namespace SampleReact.Models;

public class SqliteContext : DbContext
{
    public DbSet<Product> Products { get; set; }
    public DbSet<RiskOption> RiskOptions { get; set; }
    public DbSet<ProductOption> ProductOptions { get; set; }

    public DbSet<Agent> Agents { get; set; }

    public SqliteContext()
    {
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //optionsBuilder.UseSqlite("Filename=data.bin");
        optionsBuilder.UseSqlite("Data Source=data.bin");
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // Customize the ASP.NET Core Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Core Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);



        // Move Identity to "public" Schema:
        builder.Entity<Product>().ToTable("products");
        builder.Entity<ProductOption>().ToTable("product_options");
        builder.Entity<RiskOption>().ToTable("risk_options");
    }
}