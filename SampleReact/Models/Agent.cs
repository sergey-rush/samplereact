﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SampleReact.Models;

public class Agent : Entity<int>
{
    [Column("title")]
    public string Title { get; set; }
}