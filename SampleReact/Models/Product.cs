﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SampleReact.Models;

public class Product : Entity<int>
{
    [Column("title")]
    public string Title { get; set; }

}